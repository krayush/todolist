var spaces = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
var elementStack = [];
var checkSpanString = '<span class="check-span"><input type="checkbox"><span class="tick-mark"></span></span>';
var deleteIcon = '<button class="delete-icon"></button>';
var info = '<div class="arrow-up"></div><div class="full-info-title left-float">Task Details:&nbsp; </div><div class="full-info-text">' + spaces + '{{text}}</div><div class="clear"></div><div class="date-title left-float">Date:&nbsp; </div><div class="date-title-value"></div><div class="clear"></div>';

(function(app){
    var addEventListeners = function() {
        var addButton = document.getElementById("add-button");
        addButton.addEventListener("click", function() {
            app.getMaxID(addNewItem);
        });
    };
    
    var createRowElement = function(rowData) {
        var newFirstElement = document.createElement("div");
        var statusElement = document.createElement("div");
        var textElement = document.createElement("div");
        var deleteElement = document.createElement("div");
        var infoElement = document.createElement("div");
        var clearElement = document.createElement("div");
        newFirstElement.classList.add("list-row");
        
        textElement.classList.add("text");
        var currentDiv = document.createElement("div");
        currentDiv.classList.add("current-div");
        var spanTextDiv = document.createElement("div");
        spanTextDiv.classList.add("show-cont");
        spanTextDiv.setAttribute("pid", rowData.id);
        spanTextDiv.setAttribute("datepending", rowData.datepending);
        
        var spanText = document.createElement("span");
        spanText.innerText = rowData.todo;
        spanTextDiv.appendChild(spanText);
        currentDiv.appendChild(spanTextDiv);
        
        var editIcon = document.createElement("button");
        editIcon.classList.add('edit-icon');
        currentDiv.appendChild(editIcon);
        
        textElement.appendChild(currentDiv);
        
        var editDiv = document.createElement("div");
        var editText = document.createElement("input");
        editText.setAttribute("type", "text");
        
        var dummyEditText = document.createElement("input");
        dummyEditText.setAttribute("type", "text");
        dummyEditText.classList.add("dummy-edit-text");
        dummyEditText.disabled = true;
        
        var cancelButton = document.createElement("button");
        var saveButton = document.createElement("button");
        var dummyClear = document.createElement("div");
        editDiv.classList.add("edit-div");
        var editTextDiv = document.createElement("div");
        editTextDiv.classList.add("edit-text-div");
        editTextDiv.appendChild(editText);
        editTextDiv.appendChild(dummyEditText);
        editDiv.appendChild(editTextDiv);
        
        editDiv.appendChild(cancelButton);
        editDiv.appendChild(saveButton);
        editDiv.appendChild(dummyClear);
        saveButton.classList.add("save");
        cancelButton.classList.add("cancel");
        dummyClear.classList.add("clear");
        textElement.appendChild(editDiv);
        
        statusElement.classList.add("status");
        statusElement.title="Mark as done";
        statusElement.innerHTML = checkSpanString;
        
        deleteElement.classList.add("delete");
        deleteElement.innerHTML = deleteIcon;
        
        infoElement.classList.add("full-info");
        infoElement.innerHTML = info.replace("{{text}}", rowData.todo);
        
        clearElement.classList.add("clear");
        newFirstElement.appendChild(statusElement);
        newFirstElement.appendChild(textElement);
        newFirstElement.appendChild(deleteElement);
        newFirstElement.appendChild(infoElement);
        newFirstElement.appendChild(clearElement);
        if(rowData.datepending && rowData.datepending !== "null") {
            newFirstElement.querySelector(".date-title-value").innerText = $.datepicker.formatDate('M dd, yy',new Date(parseInt(rowData.datepending)));
        }
        if(rowData.status) {
            newFirstElement.querySelector("input[type='checkbox']").checked = true;
        }
        return newFirstElement;
    };
    
    
    var addNewItem = function(currentMaxRow) {
        var textElement = document.getElementById("new-item-text");
        var listContainer = document.getElementById("list-container");
        textElement.classList.remove("red-border");
        textElement.value = textElement.value.trim();
        if(textElement.value === "") {
            textElement.classList.add("red-border");
        } else {
            var newRowData = {todo: textElement.value, datepending: null, id: 1, status: 0};
            if(currentMaxRow && currentMaxRow.id) {
                newRowData.id = currentMaxRow.id + 1;
            }
            app.addListData(newRowData);
            elementStack.push({ value: textElement.value });
            var newFirstElement = createRowElement(newRowData);
            document.getElementById("no-more").style.display = "none";
            textElement.value = "";
            if(listContainer.firstChild) {
                listContainer.insertBefore(newFirstElement, listContainer.firstChild);
            } else {
                listContainer.appendChild(newFirstElement);
            }
        }
    };
    
    var deleteEntryFromList = function(element) {
        var mainContainer = element.parentNode.parentNode.parentNode;
        app.deleteListData(mainContainer.querySelector(".show-cont").getAttribute("pid"));
        var textElement = document.getElementById("new-item-text");
        var noMoreDiv = document.getElementById("no-more");
        if(mainContainer.children.length === 1) {
            noMoreDiv.style.display = "block";
        }
        mainContainer.removeChild(element.parentNode.parentNode);
        window.setTimeout(function (){textElement.focus();}, 1);
    };
    
    var editEntryFromList = function(element) {
        var rowContainer = element.parentNode.parentNode.parentNode;
        var value = rowContainer.querySelector(".current-div span").innerText;
        element.parentNode.style.display = "none";
        element.parentNode.nextSibling.style.display = "block";
        rowContainer.querySelector("input[type='text']").value = value;
        rowContainer.querySelector(".full-info").style.display = "none";
        initializeDatePicker(rowContainer);
    };
    
    var initializeDatePicker = function(rowContainer) {
        var dateToday = new Date(); 
        var dateElement = $(rowContainer.querySelector(".dummy-edit-text"));
        dateElement.datepicker({
            dateFormat: "M dd, yy",
            minDate: dateToday,
            onSelect: function() { $(this).data('datepicker').inline = true; },
            onClose: function() { $(this).data('datepicker').inline = false; }
        })
        setTimeout(function(){
            dateElement.datepicker("show");
        }, 500);
    };
    
    var markAsDone =  function(element) {
        var rowContainer = element.parentNode.parentNode.parentNode;
        var dataElement = rowContainer.querySelector(".show-cont");
        var id = dataElement.getAttribute("pid");
        var todo = dataElement.querySelector("span").innerText;
        var datepending = dataElement.getAttribute("datepending");
        var status = rowContainer.querySelector("input[type='checkbox']").checked ? 1 : 0;

        app.updateListData({id:id, datepending: datepending, todo: todo, status: status});
        //var datepending = dataElement.setAttribute("datepending");
    };
    
    var saveCurrentElement = function(element) {
        var rowContainer = element.parentNode.parentNode;
        var newValue = element.parentNode.querySelector("input").value;
        if(newValue.trim() !== "") {
            var dataElement = rowContainer.parentNode.querySelector(".show-cont");
            var id = dataElement.getAttribute("pid");
            var datepending = rowContainer.parentNode.querySelector(".date-title-value").innerHTML = rowContainer.parentNode.querySelector(".dummy-edit-text").value + "";
            var status = rowContainer.parentNode.querySelector("input[type='checkbox']").checked ? 1 : 0;
            
            app.updateListData({id:id, datepending: (new Date(datepending)).valueOf(), todo: newValue, status: status});
            //var datepending = dataElement.setAttribute("datepending");
            rowContainer.firstChild.querySelector("span").innerText = newValue;
            rowContainer.parentNode.querySelector(".full-info-text").innerHTML = spaces + newValue;
        }
        cancelCurrentElement(element);
    };
    
    var cancelCurrentElement = function(element) {
        element.parentNode.style.display = "none";
        element.parentNode.parentNode.firstChild.style.display = "block";
        element.parentNode.parentNode.parentNode.querySelector(".full-info").style.display = "block";
    };
    
    var eventHandler = function(e) {
        var element = e.target;
        if(element.classList.contains("delete-icon")) {
            deleteEntryFromList(element);
        }
        if(element.classList.contains("edit-icon")) {
            editEntryFromList(element);
        }
        if(element.classList.contains("save")) {
            saveCurrentElement(element);
        }
        if(element.classList.contains("cancel")) {
            cancelCurrentElement(element);
        }
        if(element.type === "checkbox") {
            markAsDone(element);
        }
    };
    
    var keyUpListener = function(e) {
        var element = e.target;
        if(element.value.length > 140) {
            element.value = element.value.substring(0, 139);
        }
    };
    
    var initApp = function() {
        var listContainer = document.getElementById("list-container");
        var textElement = document.getElementById("new-item-text");
        listContainer.addEventListener("click", eventHandler);
        listContainer.addEventListener("keyup", keyUpListener);
        textElement.addEventListener("keyup", keyUpListener);
        addEventListeners();
        setTimeout(function() {
            app.getAllListDetails(generateExistingRows);
            var sortIcon = document.getElementById("sort-icon");
            sortIcon.addEventListener("click", sortListData);
        }, 1000);
    };
    
    var sortListData = function(e) {
        var listContainer = document.getElementById("list-container");
        var element = e.target;
        listContainer.innerHTML = "";
        
        if(element.classList.contains("sorted-dv")) {
            element.classList.remove("sorted-dv");
            app.getAllListDetails(generateExistingRows);
        } else {
            element.classList.add("sorted-dv");
            app.getAllListDetails(generateExistingRows, "datepending");
        }
    };
    
    var generateExistingRows = function(rowData) {
        var listContainer = document.getElementById("list-container");
        if(rowData.length) {
            document.getElementById("no-more").style.display = "none";
            listContainer.style.display = "block";
        }
        for(var key in rowData) {
            listContainer.appendChild(createRowElement(rowData[key]));
        }
    };
    
    initApp();
})(app);