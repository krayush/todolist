var app = {};

(function(app) {
    app.DB_NAME = 'mdn-demo-indexeddb-publications';
    app.DB_VERSION = 1; // Use a long long for this value (don't use a float)
    app.DB_STORE_NAME = 'publications';
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    var request = window.indexedDB.open(app.DB_NAME, app.DB_VERSION);
    request.onsuccess = function (evt) {
        app.db = this.result;
    };
    request.onerror = function (evt) {
        console.error("openDb: ", evt.target.errorCode);
    };
    request.onupgradeneeded = function (evt) {
        var store = evt.currentTarget.result.createObjectStore(app.DB_STORE_NAME, { keyPath: 'id', autoIncrement: true });
        store.createIndex('id', 'id', { unique: true });
        store.createIndex('todo', 'todo', { unique: false });
        store.createIndex('datepending', 'datepending', { unique: false });
        store.createIndex('status', 'status', { unique: false });
    };
    var getObjectStore = function(store_name, mode) {
        var tx = app.db.transaction(store_name, mode);
        return tx.objectStore(store_name);
    };
    
    app.getMaxID = function(callback) {
        var transaction = app.db.transaction(app.DB_STORE_NAME, 'readonly');
        var objectStore = transaction.objectStore(app.DB_STORE_NAME);
        var index = objectStore.index('id');
        var openCursorRequest = index.openCursor(null, 'prev');
        var maxRevisionObject = null;
        openCursorRequest.onerror = function(event) {
            console.log(event);
        }
        openCursorRequest.onsuccess = function (event) {
            if (event.target.result) {
                maxRevisionObject = event.target.result.value; //the object with max revision
            }
        };
        transaction.oncomplete = function (event) {
            if(callback) {
                callback(maxRevisionObject);
            }
        };
    };
    
    app.addListData = function(obj) {
        var store = getObjectStore(app.DB_STORE_NAME, 'readwrite');
        var index = store.index('id');
        index.openCursor(null, 'prev');
        var req;
        try {
            req = store.add(obj);
        } catch (e) {
            if (e.name == 'DataCloneError') {
                console.log("This engine doesn't know how to clone a Blob, use Firefox");
            }
            return;
        }
        req.onsuccess = function (evt) { 
            console.log("Insertion in DB successful");
        }
        req.onerror = function(evt) {
            console.log("Error occurred while insertion");
            console.log(evt);
        };
    };

    app.deleteListData = function(key) {
        if(typeof key === "string") {
            key = parseInt(key);
        }
        var store = getObjectStore(app.DB_STORE_NAME, 'readwrite');
        var req = store.get(key);
        req.onsuccess = function(evt) {
            var record = evt.target.result;
            if (typeof record == 'undefined') {
                console.log("No matching record found");
                return;
            }
            req = store.delete(key);
            req.onsuccess = function(evt) {
                console.log("Deletion successful");
            };
            req.onerror = function (evt) {
                console.error("Delete List: ", evt.target.errorCode);
            };
        };
        req.onerror = function (evt) {
            console.error("Delete List: ", evt.target.errorCode);
        };
    }

    app.updateListData = function(obj) {
        var store = getObjectStore(app.DB_STORE_NAME, 'readwrite');
        var req = store.get(parseInt(obj.id));
        req.onsuccess = function(evt) {
            var data = req.result;
            if(data) {
                data.todo = obj.todo;
                data.datepending = obj.datepending;
                data.status = obj.status;
                var requestUpdate = store.put(data);
                requestUpdate.onerror = function(event) {
                    console.log("Do something with the error");
                };
                requestUpdate.onsuccess = function(event) {
                    console.log("Success - the data is updated!");
                };
            }
        };
        req.onerror = function (evt) {
            console.error("Update List: ", evt.target.errorCode);
        };
    }

    app.getAllListDetails = function(callback, sortColumn) {
        var store = getObjectStore(app.DB_STORE_NAME, 'readwrite');
        var cursorRequest;
        if(sortColumn) {
            cursorRequest = store.index(sortColumn).openCursor(null, 'next');
        } else {
            cursorRequest = store.index('status').openCursor(null, 'next');
        }
        var listItems = [];
        cursorRequest.onsuccess = function(evt) {
            var cursor = evt.target.result;
            if(cursor) {
                console.log(cursor.value);
                listItems.push(cursor.value);
                cursor.continue();
            } else {
                if(callback) {
                    callback(listItems);
                }
            }
        };
        cursorRequest.onerror = function (evt) {
            if(callback) {
                callback(null);
            }
            console.error("Get List: ", evt.target.errorCode);
        };
    }
})(app);